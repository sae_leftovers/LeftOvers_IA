from leftovers_ia import findRecipes

ingr_available = [389, 7655, 6270, 1527, 3406, 2683, 4969, 800, 5298, 840, 2499, 6632, 7022, 1511, 3248, 4964]

recipes_with_notes = findRecipes(ingr_available)