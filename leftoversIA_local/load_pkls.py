import numpy as np
import pandas as pd
import os
import pickle
import ast # parses list in lit string to pythong list
from tqdm import tqdm # progress bar helpful in monitoring processes

# Def Load Files func
def loadfiles(directory):
    files = {} # Initiate file dict
    for dirname, _, filenames in os.walk(directory):
        for filename in filenames:
            fullpath = os.path.join(dirname, filename)
            if filename.split(".")[-1] == "csv": # load csv file
                files[''.join(filename.split(".")[:-1])] = pd.read_csv(fullpath)
                print(f"Loaded file: {filename}")
            elif filename.split(".")[-1] == "pkl": # load pkl file
                with open(fullpath, 'rb') as f:
                    files[''.join(filename.split(".")[:-1])] =  pickle.load(f)
                    print(f"Loaded file: {filename}")
    return files

# Load files
files = loadfiles('input')

ingredients = files['ingr_map']
recipes = files['RAW_recipes']
r2i_map_raw = files['PP_recipes']

# def method to clean r2i_map_raw table
def generate_maps(r2i_map_raw):
    r2i_map = {} # key = recipe id, value = ingredient id set
    i2r_map = {} # key = ingredient id, value = recipe id set

    print("On affiche le dataframe :\n", r2i_map_raw)
    # parse and append individual rows
    for i in tqdm(range(len(r2i_map_raw.id))):
        recipe_id = r2i_map_raw.id[i]
        
        # retrieve ingredients
        ingredients = ast.literal_eval(r2i_map_raw.query("id == @recipe_id").ingredient_ids.values[0])

        # add r2i entry
        r2i_map[recipe_id] = set(ingredients)

        # add i2r entry
        for i in ingredients:
            if i in i2r_map.keys():
                i2r_map[i] = i2r_map[i].union({recipe_id})
            else:
                i2r_map[i] = {recipe_id}
    
    return r2i_map, i2r_map

r2i_map, i2r_map = generate_maps(r2i_map_raw)

i2id_map_raw_replaced = ingredients[['id','replaced']].drop_duplicates(subset='replaced', keep="first")
i2id_map_raw_raw_ingr = ingredients[['id','raw_ingr']].drop_duplicates(subset='raw_ingr', keep="first")
i2id_map_raw_processed = ingredients[['id','processed']].drop_duplicates(subset='processed', keep="first")

i2id_map = {**dict(zip(list(i2id_map_raw_replaced['replaced']), list(i2id_map_raw_replaced['id']))),
            **dict(zip(list(i2id_map_raw_raw_ingr['raw_ingr']), list(i2id_map_raw_raw_ingr['id']))),
            **dict(zip(list(i2id_map_raw_processed['processed']), list(i2id_map_raw_processed['id'])))}

id2r_map_raw = recipes[['name','id']].drop_duplicates(subset='id', keep="first")
id2r_map = dict(zip(list(id2r_map_raw['id']),list(id2r_map_raw['name'])))

r2min_map_raw = recipes[['minutes','id']].drop_duplicates(subset='id', keep="first")
r2min_map = dict(zip(list(r2min_map_raw['id']),list(r2min_map_raw['minutes'])))

with open('working/i2r_map.pkl', 'wb') as handle:
    pickle.dump(i2r_map, handle, protocol=pickle.HIGHEST_PROTOCOL)

with open('working/r2i_map.pkl', 'wb') as handle:
    pickle.dump(r2i_map, handle, protocol=pickle.HIGHEST_PROTOCOL)

with open('working/i2id_map.pkl', 'wb') as handle:
    pickle.dump(i2id_map, handle, protocol=pickle.HIGHEST_PROTOCOL)
    
with open('working/id2r_map.pkl', 'wb') as handle:
    pickle.dump(id2r_map, handle, protocol=pickle.HIGHEST_PROTOCOL)

with open('working/r2min_map.pkl', 'wb') as handle:
    pickle.dump(r2min_map, handle, protocol=pickle.HIGHEST_PROTOCOL)
