
class Recipe :
    def __init__(self, id, name, time_req, ingredients, steps):
        self.id = id
        self.name = name
        self.time_req = time_req
        self.ingredients = ingredients
        self.steps = steps

    def to_str(self):
        str_ingredients = "["
        for i in range(len(self.ingredients)):
            if (not i == 0):
                str_ingredients = str_ingredients + ", "
            str_ingredients = str_ingredients + self.ingredients[i].to_str()
        str_ingredients = str_ingredients + "]"
        return str(self.id)+" = {"+str(self.name)+", "+str(self.time_req)+", "+str_ingredients+"}"

class Ingredient :
    def __init__(self, id, name):
        self.id = id
        self.name = name

    def to_str(self):
        return str(self.id)+" = "+str(self.name)
