import pickle
from objects import Recipe, Ingredient

# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
# &&&&&&&      Parts that are gonna be replaced       &&&&&&&
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

# Loading previously generated mappers
with open('working/i2r_map.pkl', 'rb') as f:
    i2r_map =  pickle.load(f)

with open('working/r2i_map.pkl', 'rb') as f:
    r2i_map =  pickle.load(f)

with open('working/i2id_map.pkl', 'rb') as f:
    i2id_map =  pickle.load(f)

with open('working/id2r_map.pkl', 'rb') as f:
    id2r_map =  pickle.load(f)
    
with open('working/r2min_map.pkl', 'rb') as f:
    r2min_map =  pickle.load(f)

def parseIngredientList(ingredient_list_string):
    ingredient_list_id=[]
    for i in ingredient_list_string:
        ingredient_list_id.append(i2id_map[i])
    return ingredient_list_id

def getRecipesWithIngredients(available_ingredients):
    output_recipes = []
    
    for id_recipe in r2i_map:
        ingrs_for_current_recipe = r2i_map[id_recipe]
        is_recipe_valid = True
        for id_ingr in ingrs_for_current_recipe:
            if (id_ingr not in available_ingredients):
                is_recipe_valid = False
                break
        if is_recipe_valid:
            output_recipes.append(id_recipe)

    return output_recipes

def getIngredientById(id_ingredient):
    for k in i2id_map:
        if (i2id_map[k] == id_ingredient):
            return Ingredient(i2id_map[k], k)
    return None

def getRecipeById(id_recipe) :
    ingredients = []
    for ingredient in r2i_map[id_recipe]:
        ingredients.append(getIngredientById(ingredient))
    output_recipe = Recipe(id_recipe, 
                            id2r_map[id_recipe], 
                            r2min_map[id_recipe], 
                            ingredients, 
                            id2r_map[id_recipe])
    return output_recipe


# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
# &&&&&&&    Parts that ARE NOT gonna be replaced     &&&&&&&
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

def getNoteRecipe(id_recipe, available_ingredients):
    recipe = getRecipeById(id_recipe)
    note = recipe.time_req/60*100 + len(recipe.ingredients)*100
    return note

def insertInListSorted(list, tuple_to_insert):
    if (len(list) == 0):
        list.append(tuple_to_insert)
        return list
    count = 0
    while (count < len(list)) and (tuple_to_insert[1] >= list[count][1]):
        count += 1
    list.insert(count, tuple_to_insert)
    return list

def getNotesForRecipes(id_recipes, available_ingredients):
    output_data = []
    for id_recipe in id_recipes:
        insertInListSorted(output_data, (id_recipe, getNoteRecipe(id_recipe, available_ingredients)))
    return output_data

def findRecipes(available_ingredients):
    recipes_available = getRecipesWithIngredients(available_ingredients)
    print("------> Available recipes :")
    print(recipes_available)

    recipes_with_notes = getNotesForRecipes(recipes_available, available_ingredients)
    print("------> Recipes with notes :")
    print(recipes_with_notes)
    
    return recipes_with_notes
